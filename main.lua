-- Code written by Oladeji Sanyaolu 11/8/2018

require 'mouse'

love.load = function()
    mouse.load()
end

love.update = function(dt)
    mouse.update(dt)
end

love.keypressed = function(key)
    mouse.keypressed(key)
end

love.keyreleased = function(key)
    mouse.keyreleased(key)
end

love.draw = function()
    mouse.draw()
end
