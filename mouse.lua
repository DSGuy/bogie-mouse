-- Code written by Oladeji Sanyaolu 11/8/2018

mouse = {}
mouse.__index = mouse

mouseImg = {
    Dance = {
        love.graphics.newImage("res/images/mouse/normal/mouse1.png"), -- normal mouse
        love.graphics.newImage("res/images/mouse/normal/mouse2.png"), -- mouse to the right
        love.graphics.newImage("res/images/mouse/normal/mouse3.png"), -- mouse to the left
        love.graphics.newImage("res/images/mouse/normal/mouse4.png"), -- mouse to the up
        love.graphics.newImage("res/images/mouse/normal/mouse5.png"), -- mouse to the down
        love.graphics.newImage("res/images/mouse/normal/mouse6.png"), -- mouse to the left feet
        love.graphics.newImage("res/images/mouse/normal/mouse7.png"), -- mouse to the right feet 

        love.graphics.newImage("res/images/mouse/wave/mouse8.png"), -- mouse to the wave 1
        love.graphics.newImage("res/images/mouse/wave/mouse9.png"), -- mouse to the wave 2
        love.graphics.newImage("res/images/mouse/wave/mouse10.png"), -- mouse to the wave 3
        love.graphics.newImage("res/images/mouse/wave/mouse11.png"), -- mouse to the wave 4
        love.graphics.newImage("res/images/mouse/wave/mouse12.png"), -- mouse to the wave 5
        love.graphics.newImage("res/images/mouse/wave/mouse13.png"), -- mouse to the wave 6
        love.graphics.newImage("res/images/mouse/wave/mouse14.png"), -- mouse to the wave 7

        love.graphics.newImage("res/images/mouse/cross_hand_leg/mouse15.png"), -- mouse to the cross-hand-leg
        love.graphics.newImage("res/images/mouse/cross_hand_leg/mouse16.png"), -- mouse to the cross-hand-leg 2

        love.graphics.newImage("res/images/mouse/shoot/mouse17.png"), -- mouse to the shoot-down
        love.graphics.newImage("res/images/mouse/shoot/mouse18.png"), -- mouse to the shoot-down 2
        love.graphics.newImage("res/images/mouse/shoot/mouse19.png"), -- mouse to the shoot-up
        love.graphics.newImage("res/images/mouse/shoot/mouse20.png"), -- mouse to the shoot-up 2
        love.graphics.newImage("res/images/mouse/shoot/mouse21.png"), -- mouse to the hand-side
        love.graphics.newImage("res/images/mouse/shoot/mouse22.png"), -- mouse to the hand-side 2

        love.graphics.newImage("res/images/mouse/floss/mouse23.png"), -- mouse to the floss
        love.graphics.newImage("res/images/mouse/floss/mouse24.png"), -- mouse to the floss 2
        love.graphics.newImage("res/images/mouse/floss/mouse25.png"), -- mouse to the floss-back
        love.graphics.newImage("res/images/mouse/floss/mouse26.png"), -- mouse to the floss-back 2

        love.graphics.newImage("res/images/mouse/gangnam_style/mouse27.png"), -- mouse to the gangnam-style
        love.graphics.newImage("res/images/mouse/gangnam_style/mouse28.png"), -- mouse to the gangnam-style 2
        love.graphics.newImage("res/images/mouse/gangnam_style/mouse29.png"), -- mouse to the gangnam-style hand-up
        love.graphics.newImage("res/images/mouse/gangnam_style/mouse30.png"), -- mouse to the gangnam-style hand-up 2

        love.graphics.newImage("res/images/mouse/dab/mouse31.png"), -- mouse to the dab
        love.graphics.newImage("res/images/mouse/dab/mouse32.png"), -- mouse to the dab 2
        love.graphics.newImage("res/images/mouse/dab/mouse33.png"), -- mouse to the dab 3

        
    },

    Orange_Justice = {
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice1.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice2.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice3.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice4.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice5.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice6.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice7.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice8.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice9.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice10.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice11.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice12.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice13.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice14.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice15.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice16.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice17.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice13.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice12.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice11.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice10.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice9.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice8.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice7.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice6.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice5.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice4.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice3.png"),
        love.graphics.newImage("res/images/mouse/orange_justice/mouse_orangejustice2.png"),
    },

}

mouseSfx = {
    Sound = {
        "res/sfx/mouse1.wav",
        "res/sfx/mouse2.wav", -- mouse to the right
        "res/sfx/mouse3.wav", -- mouse to the left
        "res/sfx/mouse4.wav", -- mouse to the up
        "res/sfx/mouse5.wav", -- mouse to the down
        "res/sfx/mouse6-7.wav", -- mouse to the left or right feet
        "res/sfx/mouse_hand.wav", -- mouse hand up
        "res/sfx/mouse_hand_2.wav", -- mouse hand down
        "res/sfx/mouse_wave.wav", -- mouse wave
        "res/sfx/mouse16.wav", -- mouse to the cross-hand-leg 
        "res/sfx/mouse22.wav", -- mouse to the hand-side
        "res/sfx/mouse_floss.wav", -- mouse to the floss
        "res/sfx/mouse_floss_2.wav", -- mouse to the floss 2
        "res/sfx/mouse_gangnam.wav", -- mouse to the gangnam
        "res/sfx/mouse_gangnam_hand.wav", -- mouse to the gangnam hand-up
    }
}

index = 1

orange_justice = {
    index = 1,
    speed = 0,
}


mouse.load = function()
    self = setmetatable({}, mouse)
    mouse = setmetatable(self, mouse)       

    self.img = nil -- The mouse image
    mouse.setImage(1) -- Sets the mouse image
    self.x = (love.graphics.getWidth()/2)-(self.img:getWidth()/2) -- Set the mouse x position
    self.y = (love.graphics.getHeight()/2)-(self.img:getHeight()/2) -- Set the mouse y position
    self.w = 1 -- Set the mouse width
    self.h = 1 -- Set the mouse height

    mouse.audio()
end

mouse.update = function(dt)
    mouse.normal()
    mouse.hand()
    mouse.floss()
    mouse.gangnam()
    mouse.dab()
    mouse.orangejustice(8)
end

mouse.keypressed = function(key)
    mouse.wave(key)
end

mouse.keyreleased = function(key)
    if key == 'w' or key == 'q' then 
        self.snd_wave:stop()
    end
end

mouse.draw = function()
    love.graphics.draw(self.img, self.x, self.y, r, self.w, self.h)
end

mouse.audio = function()
    self.snd2 = love.audio.newSource(mouseSfx.Sound[2], 'static')
    self.snd3 = love.audio.newSource(mouseSfx.Sound[3], 'static')
    self.snd4 = love.audio.newSource(mouseSfx.Sound[4], 'static')
    self.snd5 = love.audio.newSource(mouseSfx.Sound[5], 'static')
    self.snd6_7 = love.audio.newSource(mouseSfx.Sound[6], 'static')
    self.snd_hand = love.audio.newSource(mouseSfx.Sound[7], 'static')
    self.snd_hand_2 = love.audio.newSource(mouseSfx.Sound[8], 'static')
    self.snd_wave = love.audio.newSource(mouseSfx.Sound[9], 'static')
    self.snd16 = love.audio.newSource(mouseSfx.Sound[10], 'static')
    self.snd22 = love.audio.newSource(mouseSfx.Sound[11], 'static')
    self.snd_floss = love.audio.newSource(mouseSfx.Sound[12], 'static')
    self.snd_floss_2 = love.audio.newSource(mouseSfx.Sound[13], 'static')
    self.snd_gangnam = love.audio.newSource(mouseSfx.Sound[14], 'static')
    self.snd_gangnam_hand = love.audio.newSource(mouseSfx.Sound[15], 'static')
end

mouse.normal = function()
    key = love.keyboard

    if key.isDown('up') then
       --mouse.image(mouseImg.Dance[4])
       mouse.setImage(4)
       self.snd2:play()
       index = 1
    elseif key.isDown('down') then
        --mouse.image(mouseImg.Dance[5])
        mouse.setImage(5)
        self.snd5:play()
        index = 1
    elseif key.isDown('left') then
        --mouse.image(mouseImg.Dance[3])
        mouse.setImage(3)
        self.snd3:play()
        index = 1
    elseif key.isDown('right') then
        --mouse.image(mouseImg.Dance[2])
        mouse.setImage(2)
        self.snd4:play()
        index = 1
    elseif key.isDown('z') then
        --mouse.image(mouseImg.Dance[6])
        mouse.setImage(6)
        self.snd6_7:play()
        index = 1
    elseif key.isDown('x') then
        --mouse.image(mouseImg.Dance[7])
        mouse.setImage(7)
        self.snd6_7:play()
        index = 1
    else
        if index == 1 then
            --mouse.image(mouseImg.Dance[1]) 
            mouse.setImage(1)
            self.snd2:stop()
            self.snd3:stop()
            self.snd4:stop()
            self.snd5:stop()
            self.snd6_7:stop()
            set = true
        end
    end
end

mouse.wave = function(key)
    if key == 'w' then
        if index < 7 or index > 15 then
            index = 8
            self.snd_wave:play()
            mouse.setImage(index)
        else
            index = index + 1
            self.snd_wave:play()
            mouse.setImage(index)
            if index >= 14 then
                index = 7
            end
        end
    elseif key == 'q' then
        if index < 7 or index > 15 then
            index = 8
            self.snd_wave:play()
            mouse.setImage(index)
        else
            index = index - 1
            self.snd_wave:play()
            mouse.setImage(index)
            if index <= 8 then
                index = 14
            end
        end
    else
    end
end

mouse.hand = function()
    key = love.keyboard
    if key.isDown('e') then
        self.snd_hand:play()
        mouse.setImage(19)
        index = 20
    elseif key.isDown('r') then
        self.snd_hand_2:play()
        mouse.setImage(17)
        index = 18
    elseif key.isDown('t') then -- mouse to the hand side
        mouse.setImage(16)
        self.snd16:play()
        index = 15
    elseif key.isDown('y') then
        mouse.setImage(22)
        self.snd22:play()
        index = 21
    else
        self.snd_hand:stop()
        self.snd_hand_2:stop()
        self.snd16:stop()
        self.snd22:stop()
        if index == 20 then
            mouse.setImage(20)
        elseif index == 18 then
            mouse.setImage(18)
        elseif index == 15 then
            mouse.setImage(15)
        elseif index == 21 then
            mouse.setImage(21)
        end
    end
end

mouse.floss = function()
    key = love.keyboard
    
    if key.isDown('a') then
        mouse.setImage(23)
        self.snd_floss:play()
        self.snd_floss_2:stop()
        index = 26
    elseif key.isDown('s') then
        mouse.setImage(24)
        self.snd_floss_2:play()
        self.snd_floss:stop()
        index = 25
    else
        self.snd_floss:stop()
        self.snd_floss_2:stop()
        if index == 26 then
            mouse.setImage(26)
        elseif index == 25 then
            mouse.setImage(25)
        end
    end
end

mouse.gangnam = function()
    key = love.keyboard

    if key.isDown('d') then
        mouse.setImage(27)
        self.snd_gangnam:play()
        self.snd_gangnam_hand:stop()
        index = 28
    elseif key.isDown('f') then
        mouse.setImage(29)
        self.snd_gangnam_hand:play()
        self.snd_gangnam:stop()
        index = 30
    else
        self.snd_gangnam:stop()
        self.snd_gangnam_hand:stop()
        if index == 28 then
            mouse.setImage(28)
        elseif index == 30 then
            mouse.setImage(30)
        end
    end
end

mouse.dab = function()
    key = love.keyboard

    if key.isDown('c') then
        mouse.setImage(32)
        index = 31
    elseif key.isDown('v') then
        mouse.setImage(33)
        index = 31
    else 
        if index == 31 then 
            mouse.setImage(index)
        end
    end
end

mouse.orangejustice = function(speed)
    key = love.keyboard

    if key.isDown('g') and index ~= 32 then
        index = 32
        mouse.setOrangeJustice(1)
    end

    if key.isDown('g') then
        orange_justice.speed = orange_justice.speed + 1

        if orange_justice.speed > speed then
            orange_justice.index = orange_justice.index + 1
            mouse.setOrangeJustice(orange_justice.index)
            orange_justice.speed = 0
        end

    end

    if orange_justice.index > 28 then
        orange_justice.index = 1
    end 
        
end

mouse.setImage = function(idx)
    self.img = mouseImg.Dance[idx]
end

mouse.setOrangeJustice = function(idx)
    self.img = mouseImg.Orange_Justice[idx]
end
