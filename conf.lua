-- Code written by Oladeji Sanyaolu 11/8/2018

love.conf=function(t)
    t.identity="bogiemouse"
    t.version="0.11.0"
    t.console=true
    t.author="oladejisanyaolu@gmail.com"
    t.window.title="Bogie Mouse"
    t.window.icon="res/images/icon/icon_0_x1024.png"
    t.window.width=800;
    t.window.height=600;

    t.window.borderless=false
    t.window.resizable=false;
    t.window.minwidth=1;
    t.window.minheight=1
    t.window.vsync=true;
    t.window.fsaa=0;
    t.window.display=1
    t.window.highdpi=false;
    t.window.srgb=true;

    t.modules.audio=true
    t.modules.event=true;
    t.modules.graphics=true;
    t.modules.image=true
    t.modules.joystick=false;
    t.modules.keyboard=true;
    t.modules.math=true
    t.modules.mouse=false;
    t.modules.physics=false;
    t.modules.sound=true
    t.modules.system=true;
    t.modules.timer=false;
    t.modules.window=true 
end
